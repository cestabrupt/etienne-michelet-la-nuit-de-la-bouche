// Load
window.addEventListener('load', function () {
  document.body.classList.add('loaded');
});

//Typewriter
// Source: Gavra https://codepen.io/gavra/pen/tEpzn
//

var scrollPosition = document.documentElement.scrollTop;

var aText = [...document.querySelectorAll("#typedtext p")].map(e=>e.innerHTML);

var iSpeed = 50; // time delay of print out
var iIndex = 0; // start printing array at this position
var iArrLength = aText[0].length; // the length of the text array
 
var iTextPos = 0; // initialise text position
var sContents = ''; // initialise contents variable
var iRow; // initialise current row
var option = "stop";
function typewriter(param)
{
  option = param;
  sContents =  ' ';
  iRow = 0;
  var destination = document.getElementById("apparition");
 
 while ( iRow < iIndex ) {
 sContents += '<p>' + aText[iRow++] + '</p>';
 }

  destination.innerHTML = sContents + aText[iIndex].substring(0, iTextPos) + "";

  if (option == "stop") {
    return;
  } else if (option == "refresh") {
    iIndex = 0;
    iArrLength = aText[0].length;
    iTextPos = 0;
    sContents = '';
    iRow = 0;
    option = "stop";
    typewriter(option);
  } else if ( option == "show" ) {
    option = "stop";
    typewriter(option);
  } else if ( iTextPos++ == iArrLength ) {
    iTextPos = 0;
    iIndex++;
   if ( iIndex != aText.length ) {
     iArrLength = aText[iIndex].length;
     setTimeout("typewriter(option)", iSpeed);
   }
 } else {
  setTimeout("typewriter(option)", iSpeed);
  window.scrollTo({ top: document.body.scrollHeight, behavior: 'auto' });
  scrollPosition = document.documentElement.scrollTop;
 }
}

// Musique
var audio = document.getElementById("fond-sonore");
var musiqueJoue = "pause";
function fondSonore() {
    if (audio.paused) {
        audio.play();
    }
    else {
        audio.pause();
    }
};


// Boutons
var stop = document.getElementById("play");
stop.addEventListener('click', function() {
  if (option == "stop") {
    option = "start";
    typewriter(option);
  } else if (option == "start") {
    option = "stop";
    typewriter(option);
  }
  document.querySelector('#play .icone--play').classList.toggle('none');
  document.querySelector('#play .icone--pause').classList.toggle('none');

  document.querySelector('#show .icone--show').classList.remove('none');
  document.querySelector('#show .icone--hide').classList.add('none');
  document.querySelector('#typedtext').classList.add('none');
  document.querySelector('#apparition').classList.remove('none');

  if (musiqueJoue == "continue") {
    // document.querySelector('#musique .icone--music-on').classList.toggle('none');
    // document.querySelector('#musique .icone--music-off').classList.toggle('none');
    musiqueJoue = "pause";
  } else {
    fondSonore();
    document.querySelector('#musique .icone--music-on').classList.toggle('none');
    document.querySelector('#musique .icone--music-off').classList.toggle('none');
  };
}, false);

document.querySelector('#musique').onclick = function () {
  if (musiqueJoue == "pause") {
    musiqueJoue = "continue";
  } else {
    musiqueJoue = "pause";
  };
  document.querySelector('#musique .icone--music-on').classList.toggle('none');
  document.querySelector('#musique .icone--music-off').classList.toggle('none');
  fondSonore();
}

document.querySelector('#refresh').onclick = function () {
  document.querySelector('#show .icone--show').classList.remove('none');
  document.querySelector('#show .icone--hide').classList.add('none');
  document.querySelector('#play .icone--play').classList.remove('none');
  document.querySelector('#play .icone--pause').classList.add('none');
  document.querySelector('#typedtext').classList.add('none');
  document.querySelector('#apparition').classList.remove('none');
  typewriter("refresh");

  document.querySelector('#musique .icone--music-on').classList.add('none');
  document.querySelector('#musique .icone--music-off').classList.remove('none');
  musiqueJoue = "pause";
  audio.pause();
  audio.currentTime = 0;
}

document.querySelector('#show').onclick = function () {
  if (option != "stop") {
    musiqueJoue = "continue";
    option = "stop";
    typewriter(option);
  }

  document.querySelector('#show .icone--show').classList.toggle('none');
  document.querySelector('#show .icone--hide').classList.toggle('none');
  document.querySelector('#typedtext').classList.toggle('none');
  document.querySelector('#play .icone--play').classList.remove('none');
  document.querySelector('#play .icone--pause').classList.add('none');
  document.querySelector('#apparition').classList.toggle('none');

    if (scrollPosition == 0) {
      scrollPosition = document.documentElement.scrollTop;
    }
    document.documentElement.scrollTop = scrollPosition;
}
