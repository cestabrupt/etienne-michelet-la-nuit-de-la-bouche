# ~/ABRÜPT/ETIENNE MICHELET/LA NUIT DE LA BOUCHE/*

La [page de ce livre](https://abrupt.ch/etienne-michelet/la-nuit-de-la-bouche/) sur le réseau.

## Sur le livre

La nuit est une bouche, toutes les nuits sont le corps de l'autre, et dans la bouche, il y a la nuit qui se traverse, le goût des nuits qui souffle et les ombres et les ciels, de la bouche à la bouche, le corps de l'autre qui se cristallise, sa nuit qui s'en va à la rencontre.

## Sur l'auteur

Né en 1984. Vit actuellement en France.
Écrit un journal, avec animaux, lune, fleurs et des fantômes aussi.
Enregistre sa voix, parfois, pour y voir mieux.

Son site Internet : [DONGMURI](https://www.dongmuri.net/).

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
